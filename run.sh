# The LineServer uses an Ehcache instance which will spool lines to
# disk if the LRU memory cache is exhausted.  The disk location
# can be configured in lib/ehcache.xml; the variable is
# diskStore.  It is currently set to "/mnt/cache".


java -Xms1024M -Xmx4096M -cp "lib/lineserver.jar:lib/slf4j-api-1.6.1.jar:lib/slf4j-jdk14-1.6.1.jar:lib/ehcache-core-2.5.2.jar:lib/ehcache.xml" com.hadapt.test.LineServer $1
