package com.hadapt.test;
/**
 * @author erinemiller@comcast.net
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataOutputStream;
import java.lang.StringBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sf.ehcache.CacheManager;
/**
 * Class definition.
 * Instantiate socket and cache to null, instantiate the logger.
 */
public class LineServer  {
	ServerSocket server = null;
	private static final String cacheName = "lineCache";
	private CacheManager manager= null;
	CacheWrapper cache = null;
	final Logger logger = LoggerFactory.getLogger(LineServer.class);
	
	
	/**
	 * Constructor.
	 * We don't get here unless an argument is passed to main.  
	 * Instantiate the cache Manager (singleton) and generate an instance of an Ehcache using the EhcacheWrapper class.
	 */
	public LineServer()  throws LineServerException{
				  
		try
        {
			manager = new CacheManager();
			cache = new EhcacheWrapper(cacheName, manager);
        }
		catch (Exception e) {
			throw new LineServerException (e.toString());
		}
     		
	}
	/**
	 * Main method.  Checks to make sure that we have a file name passed as the sole argument; prints usage and exits with error if not.
	 * Instantiate new LineServer instance; instantiate new FileProcessor, passing the CacheManager and CacheName to the FileProcessor.
	 * Once processing is complete, start the server.
	 * @throws LineServer Exception
	 */
	
	public static void main(String[] args) throws LineServerException {
		if (args.length != 1) {
			System.err.println("Input file must be specified as first and only argument:  LineServer fileName");
			System.exit(-1);			
		}
		LineServer lineServer = new LineServer();
		CacheManager manager = lineServer.getManager();
		String cacheName = lineServer.getCacheName();
		FileProcessor parser = new FileProcessor(args[0], manager, cacheName);
		lineServer.start();
		
	}
	
	/**
	 * Private getter for global cache manager.
	 * @returns CacheManager
	 */
	private CacheManager getManager() {
		return this.manager;
	}
	/**
	 * Private getter for global cache name.
	 * @returns String cacheName
	 */
	private String getCacheName() {
		return this.cacheName;
	}
	/**
	 * Reads from client socket to get the user request.  If the line doesn't exist in the global cache , throw an error; otherwise
	 * return "OK\r\n" and the content of the line.  
	 * Global cache is configured to spool to disk so if the line number isn't found as a key in cache, the line doesn't exist.
	 * If the user doesn't input a line number, return an error. (i.e. request equals: "GET")
	 * @param String userInput representing text from client request
	 * @throws LineServer Exception
	 */
	public String getLine(String userInput) throws LineServerException
	{
		StringBuffer content = new StringBuffer();
		if (userInput.equals("GET\n")) {
			content.append("ERR\r\n");
		}
		else if (userInput.length() > 3) {
		try {
			String lineNum = userInput.substring(4, userInput.length());
			String line = (String) cache.get((lineNum));
			if (line == null)
				content.append("ERR\r\n");
	
			else {
				content.append("OK\r\n");
				content.append(line);
			}
		}
		catch (NumberFormatException nfe) {
			throw new LineServerException(nfe.toString());
		}
	}
		return content.toString();
	
	}

	/**
	 * Start the LineServer.  Server socket runs on 10497; client socket is opened and waiting for clietn to connect.
	 * Client requests are read into an Input Stream and response is written to an Output Stream.
	 * Protocol:  GET server returns getLine.
	 * QUIT: client is disconnected.
	 * SHUTDOWN: server gracefully shuts down, cleaning up all input/output streams, buffers, etc.
	 * @throws LineServer Exception
	 */
	public void start() {
		ServerSocket server = null;
		Socket clientSocket = null;
		try {
			server = new ServerSocket(10497);
			logger.info("server listening on 10497");
			clientSocket = server.accept();
			BufferedReader inFromClient =
		               new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		    DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
		    String inputLine;
		    
			while (true) {
				try{
				    inputLine = inFromClient.readLine().toUpperCase();
					if (inputLine.startsWith("GET")) {		
						String line = getLine(inputLine);
						outToClient.writeBytes(line+"\n");
					}
					else if (inputLine.startsWith("QUIT")){
						outToClient.writeBytes("Closing client connection" + "\n");
						
						clientSocket.shutdownInput();
						clientSocket.shutdownOutput();
						inFromClient.close();
						outToClient.flush();
						outToClient.close();
						clientSocket.close();
						
					}
					else if (inputLine.startsWith("SHUTDOWN")) {
				    	logger.info("Server shutting down");
				    	outToClient.writeBytes("server shutting down, disconnecting client" + "\n");
				    	try {
				    			if (inFromClient != null) {
				    				inFromClient.close();
				    			}
				    			if (outToClient != null) {
				    				outToClient.close();
				    			}
				    			if (clientSocket != null) {
				    				clientSocket.close();
				    			}
				    			if (server != null){
				    				server.close();
				    			}
				    		System.exit(0);
				    	
				    	}
						catch (IOException ioe) {
				    		ioe.printStackTrace();
				    	}
				    	
				    	
				    }
					
					else outToClient.writeBytes("Command not understood\n");
				}
				catch (LineServerException lse) {
					logger.error(lse.toString());
				}
			    
				catch (IOException ioe) {
					return;
				}
				catch (NumberFormatException nfe) {
					nfe.printStackTrace();
					return;
				}
				    
				}
			}
					
			catch (IOException e) {
				e.printStackTrace();
			}
	    
		}

}
