package com.hadapt.test;


/**
 * Interface for defining a cache. Simple wrapper around EhCache objects; all we want to do is put and get.
 * See http://ehcache.org/documentation/recipes/wrapper
 */
public interface CacheWrapper<K, V>
{
  void put(K key, V value);

  V get(K key);
}
