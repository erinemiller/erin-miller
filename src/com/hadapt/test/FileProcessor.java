package com.hadapt.test;

/**
 * @author erinemiller@comcast.net
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sf.ehcache.CacheManager;


public class FileProcessor {
	CacheWrapper cache = null;
	final Logger logger = LoggerFactory.getLogger(FileProcessor.class);

	/**
	 * Constructor.
	 * Create a new instance of the FileProcessor, using the intputFile string as the input file to read.
	 * Instantiate global cache.
	 * @param inputFile String representing the name of the input File.
	 * @param CacheManager EhcacheManager object.
	 * @param String cacheName String representing the cache name configured in ehcache.xml
	 * @throws LineServerException
	 */
	public FileProcessor(String inputFile, CacheManager manager, String cacheName) throws LineServerException {
		
		try {
				cache = new EhcacheWrapper(cacheName, manager);
	        File f = new File(inputFile);
			if (inputFile == null) 
				throw new LineServerException ("Input file not found");
			else {
				processFile(f);
			}
		}
		//make sure that there's not some other failure, cache, etc; if there is, print error stack trace to logto log
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Reads each line from the input file into the global cache; key is the line number, value is the content.
	 * After execution, the input file has been fully processed by the Ehcache instance and every line in the file
	 * is in the cache, either spooled to indexed files on disk or in the LRU memory cache.
	 * @param inputFile String representing the name of the input File.
	 * @throws LineServerException
	 */
	public void processFile(File inputFile) throws LineServerException {

		logger.info("starting input file processing");
		BufferedReader in = null; 
		FileOutputStream fis = null;			
		String line = null;
			int lineCounter = 0;
			try {
				in = new BufferedReader(new FileReader(inputFile));
				while ((line = in.readLine()) != null) {	
					lineCounter++;
					String key = String.valueOf(lineCounter);
					cache.put(key, line);
				}
			}
			catch (FileNotFoundException fnfe) {
				throw new LineServerException(fnfe.toString());
			}
			catch (IOException ioe) {
				throw new LineServerException (ioe.toString());
			}

			finally {
				
				if (in != null) {
					try {
					in.close();
					}
					catch (IOException ioe) {
						ioe.printStackTrace();
					}
				}
				 if (fis != null) {
					try {
						fis.close();
					}
					catch (IOException ioe) {
						ioe.printStackTrace();
						
						}
					}
				 		
						
				}
			logger.info("file processing complete");

			}
			

}
