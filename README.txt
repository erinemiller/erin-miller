How does the system work?
When thinking about this problem, it seemed like a database (relational or otherwise) was one obvious way to solve it; however, that seemed like using a jackhammer on a really small nail.

Instead, I felt that getting as much of the input file into memory and then saving the rest to disk would be a more efficient way of managing this problem.

I quickly realized that I would want some sort of indexing system--I didn't want to have to read each individual line of the file from disk, I wanted some sort of hash or index that would allow me to retrieve it more efficiently.

At this point, I decided to look for a caching tool to help me--I started with JCS which proved to be old and buggy, although in some ways ideal for this problem since it is so lightweight.  I quickly abandoned it in favor of ehcache which provided me exactly what I wanted--a cache in LRU memory and the ability to spool excess data to disk.  The system basically works by first processing the file and adding each line into my Ehcache instance.  When the user asks for a line, I simply read the line from cache.

How will your system perform as the number of requests per second increases?
The system will be limited by the physical hardware.  So as you have more contention for memory, you'll start seeing CPU increase.  As there are more requests per second, there may also be more I/O load as well, for the disk-cached lines, if the file is large enough.  Of course, my goal was to read the entire file into memory if possible, to reduce disk read/writes, but depending on the size of the file, that might not be possible.  The system should perform well until the CPU is maxed out by the number of concurrent requests or the disk system is unable to keep up with the I/O load and the await time and disk utilization time increases significantly. Then response time from the LineServer will decrease.

How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?
My goal was to have the system perform the same way, regardless of the size of the file.  I tested a 140 GB file and the response time was the same; I didnt' doformal performance measurements here, but given that all content is either loaded into memory or onto indexed files on disk, the system should perform very close to the same way regardless of the file size.

What documentation, websites, papers, etc did you consult in doing this assignement?
Started off thinking about <key, value> pairs (since this is effectively a key/value store for lines of content) and did a bit of reading about non-relational databases.  Quickly realized there might be a simplier solution.
ehcache.org and the Ehcache User guides and Java APIs.
http://commons.apache.org/jcs/
Ant/JDk 1.6 APIs
Did some reading about Java concurrency to implement multi-threading for the server, but didn't implement.

What third-party libraries or other tools does the system use?
JVM 1.6 (or greater)
Ant
Ehcache caching jars
slf4j logging utilities

How long did you spend on this exercise?
Hard to say since I had so much fun....I spent some time thinking about it first before implementing; actual implementation took me about 10 -16 hours, including all documentation, testing, debugging, etc.  Actual implementation was quite simple once I had a caching framework that worked--plugging in Ehcache took me all of 30 minutes, but I spent a lot of time debugging JCS before abandoning it.

I really enjoyed this--thanks for the opportunity to play with this challenge!

