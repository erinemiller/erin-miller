# Build script for line server.
# Note that this build script assumes the following:
# Java 1.6 (or later) is installed on the machine.
# Ant is installed on the machine

# TO RUN:
# Set the $JAVA_HOME variable to your Java installation.
# Set the ANT_HOME variable to the Ant installation

#export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/Current/Commands/java
#export ANT_HOME=
export WORKING_DIR=$PWD

export PATH=$ANT_HOME/apache-ant-1.8.3/bin:$PATH

cd $WORKING_DIR

ant dist
