package com.hadapt.test;
/**
 * @author erinemiller@comcast.net
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom Exception class to capture various exceptions and log out to logger instance.
 */

public class LineServerException extends Exception {
	String _errorString;
	Error _error;
	final Logger logger = LoggerFactory.getLogger(LineServerException.class);
	
	public LineServerException(String error) {
		super(error);
		_errorString = error;
		logger.error(error);
		
	}

	public LineServerException(Error error) {
		super(error);
		_error=error;
		error.printStackTrace();
	}
	public String getErrorString() {
		return _errorString;
	}
	
	public Error getError() {
		return _error;
	}
		

}
