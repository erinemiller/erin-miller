package com.hadapt.test;
/**
 * @author erinemiller@comcast.net
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.io.DataOutputStream;

/**
 * Simple TCP client to test LineServer functionality.
 */
public class Client {
	final static String hostname = "localhost";
	final static int portNum = 10497;
	public static void main(String argv[]) throws Exception
	 {
	  
	  String commandToServer;
	  String inputLine;
	  String response;
	  Socket clientSocket = null;
	  
	  BufferedReader inFromUser = null;
	  DataOutputStream outToServer = null;
	  BufferedReader inFromServer = null;
	  
	  try {
		  // connect to the LineServer
		   clientSocket = new Socket(hostname, portNum);
		   System.out.println("connected to host on 10497");
		   // set up connection to read data from user
		   inFromUser = new BufferedReader( new InputStreamReader(System.in));
		   //output stream to write data to server
		   outToServer = new DataOutputStream(clientSocket.getOutputStream());
		   //input stream to read data from server
		   inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		   //while the user is entering input
		   while ((commandToServer = inFromUser.readLine()) != null) {		   
		   //send the server user input
		   outToServer.writeBytes(commandToServer + "\n");
		   //read response and print to std out
		   response = inFromServer.readLine();
			   System.out.println(response);
			   System.out.println(inFromServer.readLine());
		  }
	  } catch (IOException ioe) {
		  ioe.printStackTrace();
		  
	  }
	 }
	}

